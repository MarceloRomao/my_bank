const start = document.getElementById("start");
const account = document.getElementById("account");
account.style.visibility = "hidden";
const withdraw = document.getElementById("withdraw");
const newWithdrawOrLogout = document.getElementById("newWithdrawOrLogout");
const noWithdrawal = document.getElementById("noWithdrawal");
const bankNotes = document.getElementById("bankNotes");
const withdrawalAmount = document.getElementById("withdrawalAmount");
const balance = document.getElementById("balance");
const btnStart = document.getElementById("startButton");
const btnWithdraw = document.getElementById("withdrawalButton");
const btnNewWithdraw = document.getElementById("newWithdrawalButton");
const btnLogout = document.getElementById("logoutButton");

const availableBankNotesFunction = () => {
    let availableBankNotes = {
        "100": 1,
        "50": 3,
        "20": 4,
        "10": 1,
        "5": 3,
        "2": 12,
        "1": 10,
    }
    return availableBankNotes
}

const withdrawalBankNotesFunction = () => {
    let withdrawalBankNotes = {
        "100": 0,
        "50": 0,
        "20": 0,
        "10": 0,
        "5": 0,
        "2": 0,
        "1": 0,
    }
    return withdrawalBankNotes
}

let availableBankNotes = availableBankNotesFunction()
let withdrawalBankNotes = withdrawalBankNotesFunction()
let availableBankNotesLength = Object.keys(availableBankNotes).length;

let currentBalance = 122.5;
currentBalanceMoney = currentBalance.toLocaleString('pt-br',{minimumFractionDigits: 2});
const balanceText = document.createTextNode(currentBalanceMoney);
balance.appendChild(balanceText);

let currentWithdrawalAmount = 0;

const checkingBalanceAndBankNotes = (value) => {

    if (value >= 1) {
        let result = "";
        let remainingWithdrawalAmount = value;

        if (Math.floor(remainingWithdrawalAmount) != remainingWithdrawalAmount) {
            result = "Só é permitido sacar valores inteiros.";
            showMessages(result)
        }
            
        else if (remainingWithdrawalAmount > currentBalance) {
            result = "Saldo insuficiente para sacar esse valor.";
            showMessages(result)
        }
        
        else {
            let availableBankNotesValues = Object.values(availableBankNotes).reverse();
            let availableBankNotesKeys = Object.keys(availableBankNotes).reverse();
            let withdrawalBankNotesKeys = Object.keys(withdrawalBankNotes).reverse();

            for (i=0; i<availableBankNotesLength; i++) {
                let withdrawal = Math.floor(remainingWithdrawalAmount/parseInt(availableBankNotesKeys[i]))
                if (withdrawal <= availableBankNotesValues[i]) {
                    remainingWithdrawalAmount -= parseInt(availableBankNotesKeys[i]) * withdrawal;
                    withdrawalBankNotes[withdrawalBankNotesKeys[i]] = withdrawal;
                } else {
                    remainingWithdrawalAmount -= parseInt(availableBankNotesKeys[i]) * availableBankNotesValues[i];
                    withdrawalBankNotes[withdrawalBankNotesKeys[i]] = availableBankNotesValues[i];       
                }
            }
                
            if (remainingWithdrawalAmount == 0) {
                availableBankNotes["100"] -= withdrawalBankNotes["100"];
                availableBankNotes["50"] -= withdrawalBankNotes["50"];
                availableBankNotes["20"] -= withdrawalBankNotes["20"];
                availableBankNotes["10"] -= withdrawalBankNotes["10"];
                availableBankNotes["5"] -= withdrawalBankNotes["5"];
                availableBankNotes["2"] -= withdrawalBankNotes["2"];
                availableBankNotes["1"] -= withdrawalBankNotes["1"];
                       
                currentBalance -= value;
                currentBalanceMoney = currentBalance.toLocaleString('pt-br',{minimumFractionDigits: 2});
                balance.innerHTML = "";
                const balanceText = document.createTextNode(currentBalanceMoney);
                balance.appendChild(balanceText);                         
        
                result = withdrawalBankNotes;
        
                returningResults(result)
        
            } else {
                result = "Não há notas disponíveis para sacar esse valor.";        
                showMessages(result)
                }
        }   
        return result  
    }
}

const clearFields = () => {
    const noWithdrawalDestination = document.getElementById("noWithdrawal");
    noWithdrawalDestination.innerHTML = "";
    
    const oneHundredDestination = document.getElementById("100");
    oneHundredDestination.innerHTML = "";
    
    const fiftyDestination = document.getElementById("50");
    fiftyDestination.innerHTML = "";
    
    const twentyDestination = document.getElementById("20");
    twentyDestination.innerHTML = "";
    
    const tenDestination = document.getElementById("10");
    tenDestination.innerHTML = "";
    
    const fiveDestination = document.getElementById("5");
    fiveDestination.innerHTML = "";
    
    const twoDestination = document.getElementById("2");
    twoDestination.innerHTML = "";
    
    const oneDestination = document.getElementById("1");
    oneDestination.innerHTML = "";
}

const returningResults = (result) => {
    const oneHundredDestination = document.getElementById("100");
    const oneHundredText = document.createTextNode(result["100"]);
    oneHundredDestination.appendChild(oneHundredText);

    const fiftyDestination = document.getElementById("50");
    const fiftyText = document.createTextNode(result["50"]);
    fiftyDestination.appendChild(fiftyText);

    const twentyDestination = document.getElementById("20");
    const twentyText = document.createTextNode(result["20"]);
    twentyDestination.appendChild(twentyText);

    const tenDestination = document.getElementById("10");
    const tenText = document.createTextNode(result["10"]);
    tenDestination.appendChild(tenText);

    const fiveDestination = document.getElementById("5");
    const fiveText = document.createTextNode(result["5"]);
    fiveDestination.appendChild(fiveText);

    const twoDestination = document.getElementById("2");
    const twoText = document.createTextNode(result["2"]);
    twoDestination.appendChild(twoText);

    const oneDestination = document.getElementById("1");
    const oneText = document.createTextNode(result["1"]);
    oneDestination.appendChild(oneText);

    withdraw.style.visibility = "hidden";   
    newWithdrawOrLogout.style.visibility = "visible";
    noWithdrawal.style.visibility = "hidden";
    bankNotes.style.visibility = "visible";
}

showMessages = (result) => {
    const noWithdrawalDestination = document.getElementById("noWithdrawal");
    const noWithdrawalText = document.createTextNode(result);
    noWithdrawalDestination.appendChild(noWithdrawalText);
    
    withdraw.style.visibility = "hidden";
    newWithdrawOrLogout.style.visibility = "visible";
    noWithdrawal.style.visibility = "visible";
    bankNotes.style.visibility = "hidden";
}

btnStart.addEventListener("click", () => {
    start.style.display = "none";
    account.style.visibility = "visible";
    withdraw.style.visibility = "visible";
    newWithdrawOrLogout.style.visibility = "hidden";
})

btnWithdraw.addEventListener("click", () => {    
    currentWithdrawalAmount = withdrawalAmount.value;
    checkingBalanceAndBankNotes(currentWithdrawalAmount);
    withdrawalAmount.value = null;
})

btnNewWithdraw.addEventListener("click", () => {
    withdraw.style.visibility = "visible";
    newWithdrawOrLogout.style.visibility = "hidden";
    noWithdrawal.style.visibility = "hidden";
    bankNotes.style.visibility = "hidden";
    
    clearFields()
})

btnLogout.addEventListener("click", () => {
    start.style.display = "flex";
    account.style.visibility = "hidden";
    withdraw.style.visibility = "hidden";
    newWithdrawOrLogout.style.visibility = "hidden";
    noWithdrawal.style.visibility = "hidden";
    bankNotes.style.visibility = "hidden";

    clearFields()

    availableBankNotesFunction()
    withdrawalBankNotesFunction()
    
    currentBalance = 122.5;
    balance.innerHTML = "";
    currentBalanceMoney = currentBalance.toLocaleString('pt-br',{minimumFractionDigits: 2});
    const balanceText = document.createTextNode(currentBalanceMoney);
    balance.appendChild(balanceText);
    
    currentWithdrawalAmount = 0;
})

